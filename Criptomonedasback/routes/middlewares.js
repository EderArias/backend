const jwt = require('jwt-simple');
const moment = require('moment')

const checkToken = (req,res, next) => {//para todo aquel que se quiera comunicar con la app les obligamois a pasarnoslo por cabeceras
    if (!req.headers['token']){
        return res.json({ error: 'Necesitas incluir el user-token en la cabecera'});


    }

    const userToken = req.headers['token']; //necesitamos desencriptarlo
    let payload = {};
    try {
        payload = jwt.decode(userToken, 'frase secreta');//lo primero que le pasamos es que queremos decodificar y lo segundo es con que frase queremos decodificar
    } catch (err) {
        return res.json({ error: 'El token es incorrecto'});
    }
    
    if (payload.expiredAt < moment().unix()) {
        return res.json({error: 'El token ha expirado'})
    }

    req.usuarioId = payload.usuarioId; //req es el objeto que vamos a ir delegando en todos los enrutadores, con esta linea de codigo tenemos el usuario id disponible en el siguiente manejador de rutas

    next();

}

module.exports = {
    checkToken: checkToken
}