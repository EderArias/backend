const router = require('express').Router();
const bcrypt = require('bcryptjs');
const { User} = require ('../../database/db'); //importamos el modelo
const {check, validationResult } = require('express-validator'); //importamos esos metodos
//el metodo check comprueba los diferentes datos que este insetando dentro de la ruta con la que estoy trabajando, funciona como un middleware, por eso lo podemos pasar en un array antes de la callback
const moment = require('moment');
const jwt = require('jwt-simple');

router.post('/register',[
    check('username', 'El nombre de usuario es obligatorio').not().isEmpty(),
    check('password','El password es obligatorio').not().isEmpty()
], async (req,res) => {

    const errors = validationResult(req); //si le paso la peticion sobre la que estoy trabajando va a validar sobre esa peticion
    if (!errors.isEmpty()) {
        return res.status(422).json({errores: errors.array()})
    }

    req.body.password = bcrypt.hashSync(req.body.password, 10); //encriptamos la password antes de guardar el usuario, para que esta se guarde encriptada. con el metodo hashsync es para que pase la password de una vez, y el numero 10 es el numero de veces que se aplicara el algorithmo de encriptacion
    const user = await User.create(req.body);
    res.json(user);
});

router.post('/login', async (req, res) => {
    const user  = await User.findOne({ where: {username: req.body.username}}); //findone devuelve 1 si es igual y cero si no. Comprobamos que el usuario existe
    if (user){
        const iguales = bcrypt.compareSync(req.body.password, user.password); //el comparesync recibe un valor sin encriptar y un valor encriptado, en este caso el sin encriptar es el de req.body.password y el otro esta en el usuario con el que estamos comprobando
        if (iguales) {
            res.json({succes: createToken(user)}); //en el tpken puedo poner muchas cosas
        } else{
            res.json({error: 'Error en usuario y/o contraseña'});
        }
    }else{
        res.json({ error: 'Error en usuario y/o contraseña'});
    }
});

const createToken = (user) => {
    const payload = {
        usuarioId: user.id,
        createdAt: moment().unix(),
        expiredAt: moment().add(1440, 'minutes').unix()
    }

    return jwt.encode(payload, 'frase secreta'); //el metodo encode recibe el objeto payload que voy a encriptar y luego una frase secreta
}


module.exports = router;