const router = require('express').Router();

const {Monedas} = require('../../database/db') //importando un modelo

router.get('/monedasuser/:userId', async (req, res) => {
    console.log(req.usuarioId);
    const monedasu = await Monedas.findAll({
        where: {userId:req.params.userId}
    }); //con findall me recupera todos los registros dentro de la tabla
    res.json(monedasu);
});

router.get('/topmonedas/:userId',async (req,res) =>{
    const monedasu = await Monedas.findAll({
        where: {userId:req.params.userId}, 
            order: [['price', 'DESC']],
            limit: 3 
        
    });
     //con findall me recupera todos los registros dentro de la tabla
    res.json(monedasu);
})

router.post('/setmoneda', async (req, res) => {
    const monedau = await Monedas.create(req.body); //con create insertamos los datos, insertamos lo que esta dentro del parentesis
    res.json(monedau)
});//{"title":"Jurasick Park", "description":"Dinosaurios","score":4,"director":"Alguien"}

module.exports = router;