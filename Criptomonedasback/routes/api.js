const router = require('express').Router();

const middlewares = require('./middlewares');
const apiMonedasRouter = require('./api/monedaR');
const apiUsersRouter = require('./api/usersR');

router.use('/monedas', middlewares.checkToken,apiMonedasRouter);
router.use('/users', apiUsersRouter);

module.exports = router;