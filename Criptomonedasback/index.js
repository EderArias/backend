const express = require('express');
const bodyparser = require('body-parser');
require('./database/asociations');
const apiRouter = require('./routes/api');
const assetsRouter = require('./wrapper/assets');
const assetsPriceRouter = require('./wrapper/assetstickets');

var cors = require('cors');
//const testeo = express();

const app =  express();
app.use(cors({origin: 'http://localhost:4200'}));
require('./database/db');

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));

app.use('/app', apiRouter)
app.use('/app/assets', assetsRouter)
app.use('/app/assets', assetsPriceRouter)



app.listen(3000, () => {
    console.log('Servidor arrancado');
});