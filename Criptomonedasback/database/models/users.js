const sequelize = require("sequelize");

module.exports = (sequelize, type) => {
    return sequelize.define('user', { //primer parametro es el nombre de la tabla en singular, y como segundo parametro la configuracion de los campos de la tabla
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: type.STRING,
        lastname: type.STRING,
        username: type.STRING,
        favcoin: type.STRING,
        password: type.STRING(150) //un tamaño mas grande de lo habitual porque se le va a encriptar
    });
}