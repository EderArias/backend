module.exports = (sequelize, type) => {
    return sequelize.define('monedas',{
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: type.STRING,
        symbol: type.STRING,
        price: type.INTEGER
        
    });
} 