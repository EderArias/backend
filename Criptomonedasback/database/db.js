const Sequelize = require('sequelize');
const MonedasModel = require('./models/monedas'); //importamos la "funcion" o modelo
const UserModel = require('./models/users'); //importamos la "funcion" o modelo

const sequelize = new Sequelize('fullstack', 'root',null,{  //nombrebd, user, password {objeto con host y dialect}
    host: 'localhost',
    dialect: 'mysql'
});


const Monedas = MonedasModel(sequelize, Sequelize); //implementamos la funcion, el modelo Film se puede sincronizar con la base de datos
const User = UserModel(sequelize, Sequelize);

sequelize.sync({ force:false}) //esto me devuelve una promesa cuando mis tablas se hayan creado se ejecuta la funcion
    .then(() => {
        console.log('Tablas sincronizadas')
    })

module.exports = { //exportamos los objetos que necesito, en este caso film
    Monedas,
    User
}