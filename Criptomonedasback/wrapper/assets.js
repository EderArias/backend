const http = require("https");
const router = require('express').Router();

const options = {
	"method": "GET",
	"hostname": "bravenewcoin.p.rapidapi.com",
	"port": null,
	"path": "/asset?status=ACTIVE",
	"headers": {
		"x-rapidapi-key": "7c2c7742e2msh67ace4122e81ad6p151de8jsne6764bbfb2ab",
		"x-rapidapi-host": "bravenewcoin.p.rapidapi.com",
		"useQueryString": true
	}
};
var bod;
http.request(options, function (res) {
	var chunks = '';

	res.on("data", function (chunk) {
		chunks+=chunk;
	});

	res.on("end", function () {
		bod = JSON.parse(chunks);
		//console.log(bod);
	});
}).end();

//const bod = require('../assets');
router.get('/getassets', async (req,res) => {
    const films = await bod; //con findall me recupera todos los registros dentro de la tabla
    res.json(films);
});

module.exports = router;

//module.exports={
  //  bod
//}