const http = require("https");
const router = require('express').Router();

const options = {
	"method": "GET",
	"hostname": "bravenewcoin.p.rapidapi.com",
	"port": null,
	"path": "/market-cap",
	"headers": {
		"authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5EVXhNRGhHT0VReE56STVOelJCTTBJM1FrUTVOa0l4TWtRd1FrSTJSalJFTVRaR1F6QTBOZyJ9.eyJpc3MiOiJodHRwczovL2F1dGguYnJhdmVuZXdjb2luLmNvbS8iLCJzdWIiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWUBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9hcGkuYnJhdmVuZXdjb2luLmNvbSIsImlhdCI6MTYxMTA0MDIyNCwiZXhwIjoxNjExMTI2NjI0LCJhenAiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWSIsInNjb3BlIjoicmVhZDppbmRleC10aWNrZXIgcmVhZDpyYW5raW5nIHJlYWQ6bXdhIHJlYWQ6Z3dhIHJlYWQ6YWdncmVnYXRlcyByZWFkOm1hcmtldCByZWFkOmFzc2V0IHJlYWQ6b2hsY3YgcmVhZDptYXJrZXQtY2FwIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.rTTAgQONl0GEB8_oARvZbWdl7O1i3bI4WoNw4eI2kSXgrF2hvpaRIXysO-eM9D2e6EFaXAbziHm4nBWlVc8I0krRTPljF8oewVxdJ-1z-oD0rm-w_mfEp0TTf5vQeIGBXNHzcf0_CCyj7E4gdKfTe6S-hnQj-3vOO2jdYBqcmAwQy74VRFZH5rYKpmxo5bNNGOxW7tkMdXQhbufhc1UWFArzrRKLsS_WWdjE8iKmJYx6VFLhPLdn2zN2gui4HWacp7aISdW69KkoUj1u8YsgWRMHTP-SVl8eQ947rhFq2NGwESivXNorRYlMkOGu5ySDtih5Fd2p3ZwcG_vozrihJQ"

		,
		"x-rapidapi-key": "7c2c7742e2msh67ace4122e81ad6p151de8jsne6764bbfb2ab",
		"x-rapidapi-host": "bravenewcoin.p.rapidapi.com",
		"useQueryString": true
	}
};
var bod;
http.request(options, function (res) {
	var chunks = '';

	res.on("data", function (chunk) {
		chunks+=chunk;
	});

	res.on("end", function () {
		bod = JSON.parse(chunks);
		//console.log(bod);
	});
}).end();

router.get('/getassetsprice', async (req,res) => {
    const films = await bod; //con findall me recupera todos los registros dentro de la tabla
    res.json(films);
});

module.exports = router;